# To deploy

```bash
bazel build \
  //ordinary:ordinary.css \
  //overlapping_cloze:front \
  //overlapping_cloze:back

cat bazel-bin/ordinary/ordinary.css | xclip -selection clipboard
# --> paste into CSS

cat bazel-bin/overlapping_cloze/front.expanded.html | xclip -selection clipboard
# --> paste into front template

cat bazel-bin/overlapping_cloze/back.expanded.html | xclip -selection clipboard
# --> paste into back template
```
