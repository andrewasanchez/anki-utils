This directory builds the Docker image used for CI for this project.
Unfortunately Bazel only supports `amd64` right now, so this image won't work
on any other platform.

## Building

```bash
docker build -t agentydragon/anki-utils-cirunner .
```

## Pushing

```bash
docker login --username=agentydragon
docker push agentydragon/anki-utils-cirunner
```

