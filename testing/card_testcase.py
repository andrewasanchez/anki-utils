from absl import logging
from absl.testing import absltest
from deploy import deploy_lib
from io_bazel_rules_webtesting.testing.web import webtest
from rules_python.python.runfiles import runfiles
from selenium.webdriver.support.ui import WebDriverWait
import anki
import json
import os.path
import pathlib
import tempfile

# Stub out MathJax.
# TODO(prvak): Might be nice to also test MathJax rendering.
MATHJAX_STUB = """
<script>
var MathJax = {"Hub" : {"Register" : {"MessageHook" : function() {}}}};
</script>
"""


class CardTestCase(absltest.TestCase):
    tempfile_cleanup = absltest.TempFileCleanup.SUCCESS

    def setUp(self):
        self.driver = webtest.new_webdriver_session()
        self.runfiles = runfiles.Create()
        self.tmpdir = tempfile.TemporaryDirectory()

        self.collection_tempfile = self.create_tempfile('collection.anki2')
        self.collection = anki.Collection(self.collection_tempfile.full_path)

    def load_model(self, model_slug_runfiles_path):
        logging.info("Applying model slug from %s to collection...",
                     model_slug_runfiles_path)
        with open(self.runfiles.Rlocation(model_slug_runfiles_path)) as f:
            model_slug = json.load(f)

        model = deploy_lib._apply_model_update_spec(self.collection.models, {
            'mapping': {
                'name': 'Tested model'
            },
            'model': 'tested_model'
        }, {'tested_model': model_slug},
                                                    add_if_missing=True)
        self.collection.models.setCurrent(model)

    def tearDown(self):
        log = self.driver.get_log("browser")
        if len(log) > 0:
            logging.info("Browser log: %s", log)
        try:
            self.driver.quit()
        finally:
            self.driver = None

    def wait_until_loaded(self):
        def check_done(driver):
            state = driver.execute_script('return document.readyState')
            return state == 'complete'

        WebDriverWait(self.driver, 10).until(check_done)

    def open_card(self, fields, render_answer=False):
        note = self.collection.newNote()

        if 'Deck' in fields:
            deck_id = self.collection.decks.id(fields['Deck'])
            self.collection.decks.select(deck_id)
            del fields['Deck']

            note.model()["did"] = deck_id

        if 'Tags' in fields:
            note.setTagsFromStr(fields['Tags'])
            del fields['Tags']

        for name, value in fields.items():
            note[name] = value

        self.collection.addNote(note)

        if render_answer:
            rendered = note.cards()[0].a()
        else:
            rendered = note.cards()[0].q()

        html = MATHJAX_STUB + rendered
        self.open_html(html)
        self.wait_until_loaded()

    def open_html(self, html):
        tempfile = self.create_tempfile('card.html', content=html)
        logging.info("card rendered into: %s", tempfile.full_path)
        self.driver.get("file://{}".format(tempfile.full_path))

    def get_log(self):
        return self.driver.find_element_by_id('agentydragon-log').text

    def get_heading_html(self):
        return self.driver.find_element_by_tag_name('h1').get_property(
            'innerHTML')
